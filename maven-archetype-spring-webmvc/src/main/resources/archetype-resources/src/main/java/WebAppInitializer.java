package $package;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.Filter;

@SuppressWarnings("unused")
public class WebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer
{
	@Override
	protected Class<?>[] getRootConfigClasses()
	{
		return new Class<?>[] { WebConfig.class };
	}

	@Override
	protected Class<?>[] getServletConfigClasses()
	{
		return null;
	}

	@Override
	@NotNull
	protected String[] getServletMappings()
	{
		return new String[] { "/" };
	}

	@Override
	protected Filter[] getServletFilters()
	{
		return new Filter[] { new CharacterEncodingFilter("UTF-8") };
	}
}
